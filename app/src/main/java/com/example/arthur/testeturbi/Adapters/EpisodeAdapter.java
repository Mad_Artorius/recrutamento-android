package com.example.arthur.testeturbi.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.arthur.testeturbi.Model.Episode;
import com.example.arthur.testeturbi.R;

import java.util.List;

/**
 * Created by Tydenius on 14/03/2017.
 */

public class EpisodeAdapter  extends RecyclerView.Adapter<EpisodeAdapter.MyViewHolder> {

    private Context context;
    private List<Episode> episodes;


    public EpisodeAdapter(Context context, List<Episode> episodes){
        this.context = context;
        this.episodes = episodes;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtNumber;
        public TextView txtName;

        public MyViewHolder(View view) {
            super(view);

            txtNumber = (TextView) view.findViewById(R.id.episode_number);
            txtName = (TextView) view.findViewById(R.id.episode_name);

        }
    }

    @Override
    public EpisodeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_episode, parent, false);

        return new EpisodeAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Episode episode = episodes.get(position);

        holder.txtNumber.setText("E" + String.valueOf(episode.getNumber()));
        holder.txtName.setText(episode.getName());

    }

    @Override
    public int getItemCount() {
        return episodes.size();
    }


}
