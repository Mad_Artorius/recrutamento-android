package com.example.arthur.testeturbi;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.arthur.testeturbi.Adapters.EpisodeAdapter;
import com.example.arthur.testeturbi.Config.Globals;
import com.example.arthur.testeturbi.Model.Episode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ProgressDialog mProgress;
    private String show = "the-sopranos";
    private int season = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        callEpisodesApi();
        callShowApi();
    }

    public void callShowApi() {

        Future<JsonObject> jsonObjectFuture = Ion.with(this)
                .load(String.format(Globals.SHOW_API_URL, show))
                .addHeader("Content-Type", "application/json")
                .addHeader("trakt-api-version", "2")
                .addHeader("trakt-api-key", Globals.API_KEY)
                //.basicAuthentication(email, password)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        mProgress.dismiss();

                        if (e == null) {

                            JsonObject jIds = result.get("ids").getAsJsonObject();

                            getImages(jIds.get("tmdb").getAsInt());
                            getRate(jIds.get("trakt").getAsInt());


                        } else {

                            showError(e);

                        }
                    }
                });

    }

    public void callEpisodesApi() {
        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Episódios");
        mProgress.setMessage("Carregando Lista de Episódios");
        mProgress.show();

        Future<JsonArray> jsonObjectFuture = Ion.with(this)
                .load(String.format(Globals.EPISODES_API_URL, show, season))
                .addHeader("Content-Type", "application/json")
                .addHeader("trakt-api-version", "2")
                .addHeader("trakt-api-key", Globals.API_KEY)
                //.basicAuthentication(email, password)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {

                        mProgress.dismiss();

                        if (e == null) {

                            mountEpisodeList(result);

                        } else {

                            showError(e);

                        }
                    }
                });

    }

    public void mountEpisodeList(JsonArray result) {

        List<Episode> episodes = new ArrayList<>();

        for (int i = 0; i < result.size(); i++) {

            JsonObject jEpisode = result.get(i).getAsJsonObject();

            Episode episode = new Episode(jEpisode.get("number").getAsInt(), jEpisode.get("title").getAsString());
            episodes.add(episode);


        }

        RecyclerView episodeList = (RecyclerView) findViewById(R.id.episode_list);
        EpisodeAdapter adapter = new EpisodeAdapter(MainActivity.this, episodes);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        episodeList.setLayoutManager(mLayoutManager);
        episodeList.setNestedScrollingEnabled(false);
        episodeList.setItemAnimator(new DefaultItemAnimator());
        episodeList.setAdapter(adapter);

    }

    public void getRate(int traktId) {

        Future<JsonObject> jsonObjectFuture = Ion.with(this)
                .load(String.format(Globals.GET_SEASON_RATE, traktId, 1))
                .addHeader("Content-Type", "application/json")
                .addHeader("trakt-api-version", "2")
                .addHeader("trakt-api-key", Globals.API_KEY)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        if (e == null) {

                            Float rate = result.get("rating").getAsFloat();
                            TextView txtRate = (TextView) findViewById(R.id.rate);

                            txtRate.setText(String.format("%.1f", rate));


                        } else {

                            showError(e);

                        }
                    }
                });

    }

    public void getImages(int tmdbId) {

        Future<JsonObject> jsonObjectFuture = Ion.with(this)
                .load(String.format(Globals.TMDB_API_URL, tmdbId))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        mProgress.dismiss();

                        if (e == null) {

                            final ImageView showPoster = (ImageView) findViewById(R.id.show_poster);
                            final ImageView banner = (ImageView) findViewById(R.id.season_thumb);

                            showPoster.setScaleType(ImageView.ScaleType.FIT_CENTER);
                            banner.setScaleType(ImageView.ScaleType.FIT_CENTER);

                            Glide.with(MainActivity.this)
                                    .load(Globals.POSTER_URL + result.get("poster_path").getAsString())
                                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                                    .listener(new RequestListener<String, GlideDrawable>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            showPoster.setScaleType(ImageView.ScaleType.FIT_XY);
                                            return false;
                                        }
                                    })
                                    .placeholder(R.drawable.progress_animation)
                                    .dontAnimate()
                                    .into(showPoster);

                            Glide.with(MainActivity.this)
                                    .load(Globals.POSTER_URL + result.get("backdrop_path").getAsString())
                                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                                    .listener(new RequestListener<String, GlideDrawable>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            banner.setScaleType(ImageView.ScaleType.FIT_XY);
                                            return false;
                                        }
                                    })
                                    .placeholder(R.drawable.progress_animation)
                                    .dontAnimate()
                                    .into(banner);
                        } else {

                            showError(e);

                        }
                    }
                });
    }

    public void showError(Exception e) {
        Toast.makeText(this, "Não foi possível completar sua solicitação, verifique sua conexão e tente novamente ", Toast.LENGTH_SHORT).show();
    }

}
