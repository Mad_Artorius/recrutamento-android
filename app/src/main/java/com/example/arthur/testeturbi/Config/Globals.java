package com.example.arthur.testeturbi.Config;

/**
 * Created by Tydenius on 14/03/2017.
 */

public class Globals {

    public static final String SHOW_API_URL = "https://api.trakt.tv/shows/%s";
    public static final String EPISODES_API_URL = "https://api.trakt.tv/shows/%s/seasons/%d";
    public static final String API_KEY = "ddb465cb0760dfaf979e89180adf75749822d66d101bb05a16aefbe80c7e6933";

    public static final String TMDB_API_URL = "https://api.themoviedb.org/3/tv/%d?api_key=105c82645c33f9a4817882e0e74e6e5f";
    public static final String POSTER_URL = "https://image.tmdb.org/t/p/w500/";

    public static final String GET_SEASON_RATE = "https://api.trakt.tv/shows/%d/seasons/%d/ratings";

}
