package com.example.arthur.testeturbi.Model;

import java.io.Serializable;

/**
 * Created by Tydenius on 13/03/2017.
 */

public class Episode implements Serializable {

    private int number;
    private String name;

    public Episode(int number, String name){
        this.number = number;
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
